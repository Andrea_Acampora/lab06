package it.unibo.oop.lab.exception2;

public class NotEnoughFoundsException extends RuntimeException{
	public double withdraw;
	public double amount;
	
	public NotEnoughFoundsException(final double withdraw,final double amount) {
		super();
		this.amount = amount;
		this.withdraw=withdraw;
	}


	public String toString() {
		return "NotEnoughFoundsException:[ Tryng a withdraw of " + withdraw + " in a balance of " +amount+ " ]";
	}
	
	

}
