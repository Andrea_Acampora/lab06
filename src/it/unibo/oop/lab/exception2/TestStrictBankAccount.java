package it.unibo.oop.lab.exception2;

import org.junit.Test;

/**
 * JUnit test to test
 * {@link StrictBankAccount}.
 * 
 */
public class TestStrictBankAccount {

    /**
     * Used to test Exceptions on {@link StrictBankAccount}.
     */
    @Test
    public void testBankOperations() {
        /*
         * 1) Creare due StrictBankAccountImpl assegnati a due AccountHolder a
         * scelta, con ammontare iniziale pari a 10000 e nMaxATMTransactions=10
         * 
         * 2) Effetture un numero di operazioni a piacere per verificare che le
         * eccezioni create vengano lanciate soltanto quando opportuno, cioè in
         * presenza di un id utente errato, oppure al superamento del numero di
         * operazioni ATM gratuite.
         */
    	final AccountHolder a1=new AccountHolder("Andrea", "Acampora", 1);
    	final AccountHolder a2=new AccountHolder("Marco", "Rossi", 2);

    	final BankAccount b1=new StrictBankAccount(a1.getUserID(), 10000, 10);
    	final BankAccount b2=new StrictBankAccount(a2.getUserID(), 10000, 10);
    	b1.deposit(a2.getUserID(), 1000);
    	b1.withdraw(a1.getUserID(), 50);
    	//b1.withdraw(a1.getUserID(), 10050);

    }
}
