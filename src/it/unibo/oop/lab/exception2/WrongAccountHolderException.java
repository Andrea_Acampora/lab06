package it.unibo.oop.lab.exception2;

public class WrongAccountHolderException extends RuntimeException{
	/**
	 * 
	 */
	private int usrId;
	private int ba;
	

	public WrongAccountHolderException(int id, int usrID2) {
		this.usrId=id;
		this.ba=usrID2;
	}


	public String toString() {
		return "WrongAccountHolderException : [User= " + usrId + " tryng to access to Bank Account n°  " +ba+" ]";
	}
	
}
