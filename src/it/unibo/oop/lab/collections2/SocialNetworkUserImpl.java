package it.unibo.oop.lab.collections2;

import java.util.*;
/**
 * aaaaaaaa
 * Instructions
 *
 * This will be an implementation of
 * {@link it.unibo.oop.lab.collections2.SocialNetworkUser}:
 *
 * 1) complete the definition of the methods by following the suggestions
 * included in the comments below.
 *
 * @param <U>
 *            Specific user type
 */
public class SocialNetworkUserImpl<U extends User> extends UserImpl implements SocialNetworkUser<U> {

	Map<String,List<U>> people_followed=new HashMap<>();

    /*
     *
     * [FIELDS]
     *
     * Define any necessary field
     *
     * In order to save the people followed by a user organized in groups, adopt
     * a generic-type Map:
     *
     * think of what type of keys and values would best suit the requirements
     */

    /*
     * [CONSTRUCTORS]
     *
     * 1) Complete the definition of the constructor below, for building a user
     * participating in a social network, with 4 parameters, initializing:
     *
     * - firstName - lastName - username - age and every other necessary field
     *
     * 2) Define a further constructor where age is defaulted to -1
     */

    /**
     * Builds a new {@link SocialNetworkUserImpl}.
     *
     * @param name
     *            the user firstname
     * @param surname
     *            the user lastname
     * @param userAge
     *            user's age
     * @param user
     *            alias of the user, i.e. the way a user is identified on an
     *            application
     */
    public SocialNetworkUserImpl(final String name, final String surname, final String user, final int userAge) {
        super(name, surname, user, userAge);
    }

    /*
     * [METHODS]
     *
     * Implements the methods below
     */


    public boolean addFollowedUser(final String circle, final U user) {
    	if(people_followed.containsKey(circle) && people_followed.get(circle).contains(user)) {
    		System.out.println("User already exists");
    		System.exit(1);
    		}
    	else {
    		List<U> list;
    		if(!people_followed.containsKey(circle)) {
    		list=new LinkedList<U>();
    		people_followed.put(circle,list);
    		}
    		else {
    		list=people_followed.get(circle);
    		}
    		list.add(user);
    		return true;
    	}

        return false;
    }

    public Collection<U> getFollowedUsersInGroup(final String groupName) {
        return (people_followed.containsKey(groupName))?people_followed.get(groupName):new LinkedList<U>();
    }

    public List<U> getFollowedUsers() {
    	List<U> l2 = null;
    	for(List<U> l: people_followed.values()) {
    	l2.addAll(l);
    	}
        return l2;
    }

}
