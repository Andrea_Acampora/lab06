package it.unibo.oop.lab.collections1;

import java.util.*;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {

	final static private int ELEMS=100000;
	final static private int ELEMS_TO_READ=1000;

	private static final int TO_MS = 1_000_000;
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	Map<String,Long> map=new HashMap<>();
    	List<Integer> arr= new ArrayList<Integer>();
    	for(int i=1000;i<2000;i++) {
    		arr.add(i);
    	}
    	System.out.println(arr);
    	List<Integer> ll=new LinkedList<>();
    	ll.addAll(arr);
    	System.out.println(ll);
    	final Integer first=arr.get(0);
    	final Integer last=arr.get(arr.size()-1);
    	arr.set(0,last);
    	arr.set(arr.size()-1, first);
    	for(Integer i : arr) {
    		System.out.print(i+"-");
    	}
    	
        long time = System.nanoTime();
        for(int i=0;i<ELEMS;i++) {
        	arr.add(ELEMS);
        }
        System.out.println();
        System.out.println();
        time=System.nanoTime()-time;
        System.out.println("Inserting " + ELEMS + " Integers into an ArrayList took " + time
                + "ns (" + time / TO_MS + "ms)");
    	
        long time2 = System.nanoTime();
        for(int i=0;i<ELEMS;i++) {
        	ll.add(ELEMS);
        }
        
        System.out.println();
        time2=System.nanoTime()-time2;
        System.out.println("Inserting " + ELEMS + " Integers into a LinkedList took " + time2
                + "ns (" + time2 / TO_MS + "ms)");
    	
        long time3=System.nanoTime();
        for(int i=0;i<ELEMS_TO_READ;i++) {
        arr.get((arr.size()-1)/2);
        }
        time3=System.nanoTime()-time3;
        System.out.println("\nReading  " + ELEMS_TO_READ + " Integers from an ArrayList took " + time3
                + "ns (" + time3 / TO_MS + "ms)");
        
        long time4=System.nanoTime();
        for(int i=0;i<ELEMS_TO_READ;i++) {
        ll.get((ll.size()-1)/2);
        }
        time4=System.nanoTime()-time4;
        System.out.println("\nReading  " + ELEMS_TO_READ + " Integers from an a LinkedList took " + time4
                + "ns (" + time4 / TO_MS + "ms)");
        map.put("Africa",1110635000L);
        map.put("Americas", 972005L);
        map.put("Antarctica", 0L);
        map.put("Asia", 4298723000L);
        map.put("Europa", 742452000L);
        map.put("Oceania", 38304000L);
        Long tot_pop = 0L;
        for(Long l : map.values()) {
        	tot_pop=tot_pop+l;
        }
        System.out.println("\nNumber of people in the world: "+tot_pop);
    	/*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    }
}
